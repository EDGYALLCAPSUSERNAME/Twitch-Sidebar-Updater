# This is the config file for the sidebar because it can
# only be updated all at once, this will store the markdown
# used around the twitch streams section

SB_TOP = '''
Replace with top section of sidebar\n\n
'''

SB_BOTTOM = '''
Replace with bottom section of sidebar
'''

# this is how you will want each streamer formatted
# if you want to rearrange or change formatting, you must
# leave the {variable_name} as they are to have it the
# data to be put in correctly
LS_SECTION = '[{stream_title}]({link})\n\n{streamer_name} Viewers: {viewers}'