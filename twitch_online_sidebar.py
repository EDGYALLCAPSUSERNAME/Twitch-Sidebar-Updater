import time
import praw
import requests
import OAuth2Util
import sb_config as sb

# User config
# --------------------------------------------------------------------
# Don't include the /r/
SUBREDDIT_NAME = ''

# The names of the twitch channels you want to be in the sidebar
TWITCH_CHANNELS = []

# --------------------------------------------------------------------


def get_online_channels():
    url = 'https://api.twitch.tv/kraken/streams/{}'
    online = []

    print('Getting online channels...')
    # go through each channel and see if it's online
    for channel in TWITCH_CHANNELS:
        resp = requests.get(url.format(channel))
        js = resp.json()
        # will be null if they aren't online
        if js['stream']:
            online.append(js)

    return online


def update_sidebar(r, online):
    ol_streamers = ''
    for channel in online:
        title = channel['stream']['channel']['status']
        link = channel['stream']['channel']['url']
        streamer = channel['stream']['channel']['display_name']
        viewers = channel['stream']['viewers']

        ol_streamers += sb.LS_SECTION.format(stream_title=title,
                                             link=link,
                                             streamer_name=streamer,
                                             viewers=viewers) + '\n\n'

    sidebar_string = sb.SB_TOP + ol_streamers + sb.SB_BOTTOM

    print('Updating sidebar...')
    sub = r.get_subreddit(SUBREDDIT_NAME)
    r.update_settings(sub, description=sidebar_string)


def main():
    r = praw.Reddit(user_agent='twitch_sidebar_online v1.0 /u/cutety')
    o = OAuth2Util.OAuth2Util(r, print_log=True)

    while True:
        try:
            online = get_online_channels()
            update_sidebar(r, online)
        except Exception as e:
            print('ERROR: {}'.format(e))

        # sleep 15 minutes
        print('Sleeping...')
        time.sleep(900)


if __name__ == '__main__':
    main()